set head off pages 0 lines 200 trimout on trimspool on feed off sqlblanklines off
-- fetch latch size
variable latch_size number;
begin
 select max(ksmfssiz) into :latch_size from x$ksmfsv where ksmfsnam like '%_latch_';
end;
/
spool memory_ranges.csv
-- format: start address (int) | end address (int) | memory area (redo,fixed,variable) | description
-- public redo 
select to_number(rawtohex(first_buf_kcrfa),'xxxxxxxxxxxxxxxx')||'|'||(to_number(rawtohex(first_buf_kcrfa),'xxxxxxxxxxxxxxxx')+strand_size_kcrfa)||'|'||'redo'||'|'||'PUB_REDO_'||indx from x$kcrfstrand where pnext_buf_kcrfa_cln != hextoraw(0);

-- private redo strands
select to_number(rawtohex(ptr_kcrf_pvt_strand),'xxxxxxxxxxxxxxxx')||'|'||(to_number(rawtohex(ptr_kcrf_pvt_strand),'xxxxxxxxxxxxxxxx')+strand_size_kcrfa)||'|'||'shared pool'||'|'||'PVT_REDO_'||indx from x$kcrfstrand where ptr_kcrf_pvt_strand != hextoraw(0);

-- in memory undo buffers
select to_number(rawtohex(ktifpupb),'xxxxxxxxxxxxxxxx')||'|'||(to_number(rawtohex(ktifpupb),'xxxxxxxxxxxxxxxx')+ktifppsi)||'|'||'shared pool'||'|'||'IMU_'||indx from x$ktifp;

-- fixed sga variables
select to_number(rawtohex(ksmfsadr),'xxxxxxxxxxxxxxxx')||'|'||(to_number(rawtohex(ksmfsadr),'xxxxxxxxxxxxxxxx')+ksmfssiz)||'|'||'fixed sga'||'|'||'var:'||ksmfsnam from x$ksmfsv;

-- shared pool/ksmsp
select to_number(rawtohex(ksmchptr),'xxxxxxxxxxxxxxxx')||'|'||(to_number(rawtohex(ksmchptr),'xxxxxxxxxxxxxxxx')+ksmchsiz)||'|'||'shared pool'||'|'||ksmchcom||',duration '||ksmchdur||',cls '||ksmchcls from x$ksmsp;

-- shared pool/ksmhp (subheaps)
select to_number(rawtohex(h.ksmchptr),'xxxxxxxxxxxxxxxx')||'|'||(to_number(rawtohex(h.ksmchptr),'xxxxxxxxxxxxxxxx')+h.ksmchsiz)||'|'||'shared pool'||'|'||s.ksmchcom||' DS:'||h.ksmchcom||',duration '||s.ksmchdur||',cls '||h.ksmchcls from (select unique ksmchpar, ksmchcom, ksmchdur from x$ksmsp where ksmchpar != hextoraw('00')) s, x$ksmhp h where s.ksmchpar=h.ksmchds;

-- shared pool reserved/ksmspr
select to_number(rawtohex(ksmchptr),'xxxxxxxxxxxxxxxx')||'|'||(to_number(rawtohex(ksmchptr),'xxxxxxxxxxxxxxxx')+ksmchsiz)||'|'||'shared pool reserved'||'|'||ksmchcom||',cls '||ksmchcls from x$ksmspr;

-- latches parent latches 
select to_number(rawtohex(kslltaddr),'xxxxxxxxxxxxxxxx')||'|'||(to_number(rawtohex(kslltaddr),'xxxxxxxxxxxxxxxx')+:latch_size)||'|'||'fixed sga'||'|'||'(parent)latch:'||kslltnam from x$kslltr_parent;

-- latches child latches
select to_number(rawtohex(kslltaddr),'xxxxxxxxxxxxxxxx')||'|'||(to_number(rawtohex(kslltaddr),'xxxxxxxxxxxxxxxx')+:latch_size)||'|'||'shared pool'||'|'||'(child)latch#'||kslltcnm||':'||kslltnam from x$kslltr_children;

-- buffercache buffers
select to_number(rawtohex(ba),'xxxxxxxxxxxxxxxx')||'|'||(to_number(rawtohex(ba),'xxxxxxxxxxxxxxxx')+blsiz)||'|'||'buffer cache'||'|'||ltrim(rawtohex(ba),'0') from x$bh;

-- dictionary cache parent
select to_number(rawtohex(p.addr),'xxxxxxxxxxxxxxxx')||'|'||(to_number(rawtohex(p.addr),'xxxxxxxxxxxxxxxx')+p.kqrpdtsz)||'|'||'shared pool'||'|(parent dc)'||s.kqrsttxt from x$kqrpd p, x$kqrst s where s.kqrstcid = p.kqrpdcid;

-- dictionary cache subordinate
select to_number(rawtohex(p.addr),'xxxxxxxxxxxxxxxx')||'|'||(to_number(rawtohex(p.addr),'xxxxxxxxxxxxxxxx')+p.kqrsdtsz)||'|'||'shared pool'||'|(subordinate dc)'||s.kqrsttxt from x$kqrsd p, x$kqrst s where s.kqrstcid = p.kqrsdcid;

-- ipc info from x$ksmssinfo >> oracle 12 only <<
select to_number(rawtohex("START ADDR"),'xxxxxxxxxxxxxxxx')||'|'||(to_number(rawtohex("START ADDR"),'xxxxxxxxxxxxxxxx')+"SIZE")||'|'||"AREA NAME"||'('||("PAGESIZE"/1024)||'k)'||'|' from x$ksmssinfo;

spool off
