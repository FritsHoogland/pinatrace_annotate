#!/bin/bash
#
# Pinatrace version 3.
#
# generate lookup table for oracle executable symbols

echo "generate code symbols from $ORACLE_HOME/bin/oracle."
nm -t d -SC $ORACLE_HOME/bin/oracle | grep ' [Tt] ' | sort | awk '{ if ( $4 != "" ) { printf "%d|%d|exec:code sym|%s\n", $1, $1+$2-1, $4 } }' > oracle.code.csv
echo "generate data symbols from $ORACLE_HOME/bin/oracle."
nm -t d -SC $ORACLE_HOME/bin/oracle | grep ' [Dd] ' | sort | awk '{ if ( $4 != "" ) { printf "%d|%d|exec:data sym|%s\n", $1, $1+$2-1, $4 } }' > oracle.data.csv
echo "generate rodata symbols from $ORACLE_HOME/bin/oracle."
nm -t d -SC $ORACLE_HOME/bin/oracle | grep ' [Rr] ' | sort | awk '{ if ( $4 != "" ) { printf "%d|%d|exec:rodata sym|%s\n", $1, $1+$2-1, $4 } }' > oracle.rodata.csv
echo "generate weak symbols from $ORACLE_HOME/bin/oracle."
nm -t d -SC $ORACLE_HOME/bin/oracle | grep ' [Vv] ' | sort | awk '{ if ( $4 != "" ) { printf "%d|%d|exec:weak sym|%s\n", $1, $1+$2-1, $4 } }' > oracle.weak.csv
echo "create table memory_ranges."
sqlite3 pinatrace.db "drop table if exists memory_ranges;"
sqlite3 pinatrace.db "create table memory_ranges (start_address integer, end_address integer, memory_area varchar(30), description varchar(100));"
echo "import code symbols."
sqlite3 pinatrace.db ".import oracle.code.csv memory_ranges"
echo "import data symbols."
sqlite3 pinatrace.db ".import oracle.data.csv memory_ranges"
# these values look fishy
sqlite3 pinatrace.db "delete from memory_ranges where start_address in (0,8,16);"
echo "import rodata symbols."
sqlite3 pinatrace.db ".import oracle.rodata.csv memory_ranges"
echo "import weak symbols."
sqlite3 pinatrace.db ".import oracle.weak.csv memory_ranges"
echo "import memory_ranges (from oracle script)."
sqlite3 pinatrace.db ".import memory_ranges.csv memory_ranges"
echo "import memory_ranges_xtables (from oracle script)."
sqlite3 pinatrace.db ".import memory_ranges_xtables.csv memory_ranges"
echo "import memory_ranges_pga (from oracle script)."
sqlite3 pinatrace.db ".import memory_ranges_pga.csv memory_ranges"
echo "create index."
sqlite3 pinatrace.db "create index if not exists memory_range_start_address on memory_ranges (start_address, end_address, memory_area, description);"
echo "remove duplicate rows."
sqlite3 pinatrace.db "delete from memory_ranges where rowid in (select a.rowid from memory_ranges a, memory_ranges b where a.rowid > b.rowid and a.start_address=b.start_address and a.end_address=b.end_address and a.memory_area=b.memory_area and a.description=b.description)"
echo "reindex."
sqlite3 pinatrace.db "reindex memory_range_start_address"
