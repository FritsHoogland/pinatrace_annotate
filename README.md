Pintools access trace annotate for Oracle databases.
Version 3.

This annotates the typical output of a pinatrace, eg.:
<pre>
0x0000000011e2991e: R 0x00007f22b5342c18  8         0x816a7c60
0x0000000011e2992a: W 0x00000000816a84e0  4          0x3200e14
0x0000000011e29931: R 0x00000000816a8510  1                  0
0x0000000011e2993a: R 0x00007ffe03d41208  8         0x816a8f10
0x0000000011e2993e: R 0x00007ffe03d41218  8         0x816a8f10
0x0000000011e29942: R 0x00007ffe03d41210  8         0x816a8f10
0x0000000011e29946: R 0x00007ffe03d41220  8     0x7f22b5351980
0x0000000011e2994a: R 0x00007ffe03d41200  8     0x7ffe03d413a8
0x0000000011e29951: R 0x00007ffe03d41230  8     0x7ffe03d41390
0x0000000011e29952: R 0x00007ffe03d41238  8         0x11e27659
</pre>
To be annotated like this:
<pre>
261 kslwt_end_snapshot+190 R 0x00007f5a65dc4c18() 8 2169283280/0x814ca2d0(Variable Size(pgsz:4k)|+541893328shared pool|permanent memor,duration 1,cls perm+4182736shared pool|(indx:13)X$KSURU+0shared pool|(indx:13)X$KSUSE+0shared pool|(indx:13)X$KSUSECST+0shared pool|(indx:13)X$KSUSIO+0shared pool|(indx:13)X$KSUSM+0)
262 kslwt_end_snapshot+202 W 0x00000000814cab50(Variable Size(pgsz:4k)|+541895504shared pool|permanent memor,duration 1,cls perm+4184912shared pool|(indx:13)X$KSUSE.KSUSETIM+0shared pool|(indx:13)X$KSUSECST.KSUSSTIM+0) 4 23311207/0x163b367(exec:code sym|kjrfnd+5159)
263 kslwt_end_snapshot+209 R 0x00000000814cab80(Variable Size(pgsz:4k)|+541895552shared pool|permanent memor,duration 1,cls perm+4184960) 1 0/0
264 kslwt_end_snapshot+218 R 0x00007fffec9a2408 8 2169288064/0x814cb580(Variable Size(pgsz:4k)|+541898112shared pool|permanent memor,duration 1,cls perm+4187520)
265 kslwt_end_snapshot+222 R 0x00007fffec9a2418 8 2169288064/0x814cb580(Variable Size(pgsz:4k)|+541898112shared pool|permanent memor,duration 1,cls perm+4187520)
266 kslwt_end_snapshot+226 R 0x00007fffec9a2410 8 2169288064/0x814cb580(Variable Size(pgsz:4k)|+541898112shared pool|permanent memor,duration 1,cls perm+4187520)
267 kslwt_end_snapshot+230 R 0x00007fffec9a2420 8 140026232781184/0x7f5a65dd3980()
268 kslwt_end_snapshot+234 R 0x00007fffec9a2400 8 140737162913192/0x7fffec9a25a8
269 kslwt_end_snapshot+241 R 0x00007fffec9a2430 8 140737162913168/0x7fffec9a2590
270 kslwt_end_snapshot+242 R 0x00007fffec9a2438 8 300054105/0x11e27659(exec:code sym|kslwtectx+425)
</pre>
This is can be done in the following way:
1. Log on as oracle (the oracle database owner) to a linux server.
2. Clone the pinatrace_annotate repository (git clone https://gitlab.com/FritsHoogland/pinatrace_annotate.git).
3. Start a session to be traced with pinatrace.
4. Start a SYSDBA session from the pinatrace_annotate repo directory you just cloned.
5. @get_memory_ranges
6. The get_memory_ranges.sql script will run for a while, then list all the sessions and ask for a pid. Enter the pid of the session to be traced.
7. In order for the v$process_memory_detail to be populated, execute something in the session for which the pid was filled out, then press enter.
8. Exit the SYSDBA session, now create an sqlite database from the memory ranges that have been collected: ./create_database.sh 

At this point the pinatrace_annotate.awk tool is ready. Create a pinatrace using the intel pin tools, move the pinatrace file into the pinatrace_annotate directory, and use the annotation tool:
<pre>
./pinatrace_annotate_oracle.awk pinatrace.txt > pinatrace_annotated.txt
</pre>
This tool is needs the following tools, and is tested with the versions listed:
- oracle database 
- awk version 4.0.2
- sqlite 3.7.17

Sometimes it's handy to have the tracing data in a database so you can use SQL to query the output.
If you want that, change the variable annotate_database_creation value from 0 to 1. 
The script will now create a sqlite database 'annotate.db', and insert all the tracing lines into that database.
This makes it easy to for example search for a range (using the line numbers), and only writes (using the read_write field).

Despite the fact that this awk script is faster than the original bash script, it still takes a considerable amount of time to process a pinatrace file.
This is mainly because of sqlite taking a long time, in my usage I see sqlite running 100 on a cpu thread.
Any suggestion for speeding this up further are welcome.

Happy tracing!

Frits Hoogland (frits.hoogland@gmail.com)
