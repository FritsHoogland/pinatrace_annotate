#!/bin/awk -f
function cache_memory_address(address) {
  if ( strtonum(address) >= min_mem_address && strtonum(address) <= max_mem_address ) {
    mem_cache[address] = address "(";
    annotation_gather = "";
    print "select memory_area, description, start_address from memory_ranges where "strtonum(address)" between start_address and end_address-1;" |& sqlite_execute;
    print "select 'zzzzzz';" |& sqlite_execute
    while ( (sqlite_execute |& getline line) > 0 ) {
      if ( line == "zzzzzz" ) break;
      memory_address = line;
      sub(/.*\|/,"",memory_address);
      memory_address = strtonum(address) - memory_address;
      memory_annotation = line;
      sub(/\|[0-9]*$/,"",memory_annotation);
      annotation_gather = annotation_gather memory_annotation "+" memory_address;
    }
    mem_cache[address] = mem_cache[address] annotation_gather ")";
  } else {
  mem_cache[address] = address;
  }
}
function cache_ip_address(address) {
  if ( strtonum(address) >= min_exec_address && strtonum(address) <= max_exec_address ) {
    print "select case when count(1) = 0 then '??' end, description, start_address from memory_ranges where "strtonum(address)" between start_address and end_address limit 1;" |& sqlite_execute;
    sqlite_execute |& getline ip_cache[address];
    if ( ip_cache[address] ~ /^\?\?/ ) {
      sub(/\|\|/,"",ip_cache[address]);
    } else {
      sub(/^\|/,"",ip_cache[address]);
      start_address = ip_cache[address];
      sub(/.*\|/,"",start_address);
      start_address = strtonum(address) - start_address;
      sub(/\|[0-9]*$/,"+"start_address,ip_cache[address]);
    }  
  } else {
    ip_cache[address] = "??";
  }
}
  
BEGIN {
  annotate_database_creation = 0;
  sqlite_execute = "sqlite3 pinatrace.db";
  if ( annotate_database_creation == 1 ) {
    sqlite_annotate = "sqlite3 annotate.db";
    print "drop table if exists annotation;" |& sqlite_annotate;
    print "create table annotation ( line integer, function_offset varchar(100), memory_address integer, memory_annotation varchar(1000), read_write char(1), size integer, value integer, value_annotation varchar(1000));" |& sqlite_annotate;
  }
  print "select min(start_address) from memory_ranges;" |& sqlite_execute;
  sqlite_execute |& getline min_mem_address;
  print "select max(end_address) from memory_ranges;" |& sqlite_execute;
  sqlite_execute |& getline max_mem_address;
  print "select min(start_address) from memory_ranges where memory_area = 'exec:code sym';" |& sqlite_execute;
  sqlite_execute |& getline min_exec_address;
  print "select max(end_address) from memory_ranges where memory_area = 'exec:code sym';" |& sqlite_execute;
  sqlite_execute |& getline max_exec_address;
  output_row_nr=1;
  print "pragma cache_size = 409600;" |& sqlite_execute;
}
{
  # skip line if it start with #
  if ( match($0,/^#/) ) next;

  # lookup the instruction pointer
  sub(/:$/,"",$1);
  if ( ip_cache[$1] == "" ) cache_ip_address($1);
  # lookup the memory address
  if ( mem_cache[$3] == "" ) cache_memory_address($3);
  # lookup the value address
  if ( mem_cache[$5] == "" ) cache_memory_address($5);

  # output all the information for this row
  print output_row_nr, ip_cache[$1], $2, mem_cache[$3], $4, strtonum($5) "/" mem_cache[$5];
  if ( annotate_database_creation == 1 ) print "insert into annotation ( line, function_offset, memory_address, memory_annotation, read_write, size, value, value_annotation ) values ( "output_row_nr",'"ip_cache[$1]"',"strtonum($3)",'"mem_cache[$3]"','"$2"',"$4","strtonum($5)",'"mem_cache[$5]"' );" |& sqlite_annotate;
  output_row_nr++;
}
END {
  close(sqlite_execute);
  close(sqlite_annotate);
}
